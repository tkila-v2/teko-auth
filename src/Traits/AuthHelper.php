<?php

namespace TekoEstudio\Auth\Traits;

use TekoEstudio\Auth\Exceptions\AuthError\AuthJwtTokenInvalidException;
use TekoEstudio\Auth\Models\UserModel;
use function password_hash;
use const PASSWORD_DEFAULT;

trait AuthHelper
{
    /**
     * @return \TekoEstudio\Auth\Traits\Auth
     */
    private function getAuth(): Auth
    {
        return Auth::getInstance();
    }

    /**
     * Get primary key at current user logged
     *
     * @return int
     * @throws \TekoEstudio\Auth\Exceptions\AuthError\AuthJwtTokenInvalidException
     */
    public function getAuthUserId(): int
    {
        $id = $this->getAuth()->user()->getId();
        return $id > 0 && $id != null ? $id : throw new AuthJwtTokenInvalidException();
    }

    /**
     * Disable a user account from primary key
     *
     * @param int $id
     *
     * @return void
     */
    public function disableUserAccount(int $id)
    {
        UserModel::where('id', $id)->update(['status' => 0]);
    }

    /**
     * Get current user model
     *
     * @param int|null $id
     *
     * @return \TekoEstudio\Auth\Models\UserModel
     * @throws \TekoEstudio\Auth\Exceptions\AuthError\AuthJwtTokenInvalidException
     */
    public function getUser(int|null $id = null): UserModel
    {
        return UserModel::where('id', $id ?? $this->getAuthUserId())->first();
    }

    /**
     * @return string
     * @throws \TekoEstudio\Auth\Exceptions\AuthError\AuthJwtTokenInvalidException
     */
    public function getUserRole(): string
    {
        return (string) $this->getUser()->getAttribute('role');
    }

    /**
     * @param string $email
     * @param string $password
     * @param string $username
     * @param string $role
     * @param bool   $verify
     * @param array  $extra
     *
     * @return \TekoEstudio\Auth\Models\UserModel
     */
    public function newAccount(string $email, string $password, string $username, string $role = 'user', bool $verify = false, array $extra = []): UserModel
    {
//        $extraVerified = ['verified' => $verify ? 1 : 0];
//        $extraParams   = array_merge($extraVerified, $extra);
//        $response      = $this->getAuth()->tk_register($email, $password, $role, $username, $verify, $extraParams);
//
//        // When user is not registered
//        if ($response['error']) {
//            throw new RegisterAccountException($response['message'], type : $response['type']);
//        }
//
//        return UserModel::where('email', $email)->first();
        return $this->getAuth()->user()->getModel();
    }

    /**
     * @param string|null $email
     * @param string|null $password
     * @param string|null $username
     * @param int|null    $id
     *
     * @return \TekoEstudio\Auth\Models\UserModel
     * @throws \TekoEstudio\Auth\Exceptions\AuthError\AuthJwtTokenInvalidException
     */
    public function updateAccount(string $email = null, string $password = null, string $username = null, int $id = null): UserModel
    {
        $user   = $this->getUser($id);
        $params = [
            'email'    => $email ?? $user->getAttribute('email'),
            'username' => $username ?? $user->getAttribute('username'),
            'password' => $password != null ? $this->hashPassword($password) : $user->getAttribute('password')
        ];

        $user->update($params);
        return $user->fresh();
    }

    /**
     * @param string $password
     *
     * @return string
     */
    private function hashPassword(string $password): string
    {
        return password_hash($password, PASSWORD_DEFAULT);
    }
}