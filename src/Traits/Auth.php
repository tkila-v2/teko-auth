<?php

namespace TekoEstudio\Auth\Traits;

use Exception;
use TekoEstudio\Auth\Schemes\UserScheme;

class Auth
{
    /**
     * @var array
     */
    private static array $instances = [];

    /**
     * @var \TekoEstudio\Auth\Schemes\UserScheme|null
     */
    private ?UserScheme $user = null;

    /**
     * Constructor
     */
    protected function __construct()
    {
        // TODO.
    }

    /**
     * @return void
     */
    protected function __clone()
    {
        // TODO.
    }

    /**
     * @throws \Exception
     */
    public function __wakeup()
    {
        throw new Exception('Cannot un serialize a singleton.');
    }

    /**
     * @return \TekoEstudio\Auth\Traits\Auth
     */
    public static function getInstance(): Auth
    {
        $cls = static::class;
        if (!isset(self::$instances[$cls])) {
            self::$instances[$cls] = new static();
        }

        return self::$instances[$cls];
    }

    /**
     * @param \TekoEstudio\Auth\Schemes\UserScheme $userScheme
     *
     * @return void
     */
    public function setUser(UserScheme $userScheme)
    {
        $this->user = $userScheme;
    }

    /**
     * @return \TekoEstudio\Auth\Schemes\UserScheme|null
     */
    public function user(): ?UserScheme
    {
        return $this->user;
    }
}
