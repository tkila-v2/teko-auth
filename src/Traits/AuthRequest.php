<?php

declare(strict_types = 1);

namespace TekoEstudio\Auth\Traits;

use Exception;
use support\AuthJWT;
use TekoEstudio\Auth\Schemes\UserScheme;

trait AuthRequest
{
    /**
     * @return void
     * @throws \Exception
     */
    public function authCheck()
    {
        $token = $this->getToken();

        // Token not found
        if (is_null($token)) {
            throw new Exception('Token required');
        }

        AuthJWT::Check($token);
        Auth::getInstance()->setUser(
            $this->user()
        );
    }

    /**
     * @return \TekoEstudio\Auth\Schemes\UserScheme|null
     * @throws \TekoEstudio\Auth\Exceptions\AuthError\AuthJwtTokenInvalidException
     */
    public function user(): ?UserScheme
    {
        $token = $this->getToken();

        return is_null($token) ? null : new UserScheme(
            $this->getToken()
        );
    }

    /**
     * @return string|null
     * @noinspection PhpUndefinedClassInspection
     */
    public function getToken(): ?string
    {
        $authorization = parent::header('authorization');

        if (!empty($authorization)) {
            list($token) = sscanf($authorization, 'Bearer %s');
            return $token;
        }

        return null;
    }
}