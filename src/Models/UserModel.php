<?php

namespace TekoEstudio\Auth\Models;

use Illuminate\Database\Eloquent\Model;

class UserModel extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['*'];

    /**
     * Hide specific attributes
     *
     * @var array
     */
    protected $hidden = [
        'password'
    ];

    /**
     * Table name
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
}