<?php

namespace TekoEstudio\Auth\Implementations;

use Delight\Auth\Auth as AuthManager;
use Delight\Auth\EmailNotVerifiedException;
use Delight\Auth\InvalidEmailException;
use Delight\Auth\InvalidPasswordException;
use Delight\Auth\InvalidSelectorTokenPairException;
use Delight\Auth\NotLoggedInException;
use Delight\Auth\TokenExpiredException;
use Delight\Auth\TooManyRequestsException;
use Delight\Auth\UnknownUsernameException;
use Delight\Auth\UserAlreadyExistsException;
use PDO;
use TekoEstudio\Auth\Models\UserModel;
use Throwable;

class Auth
{
    private AuthManager $auth;

    public function __construct()
    {
        $db_user = env('DB_USERNAME');
        $db_pass = env('DB_PASSWORD');
        $db_name = env('DB_DATABASE');
        $db_host = env('DB_HOST');
        try {
            $pdo = new PDO("mysql:dbname=$db_name;host=$db_host;charset=utf8mb4", $db_user, $db_pass, []);
        } catch (Throwable $th) {
            return json(['error' => TRUE, 'message' => 'La conexión con la base de datos falló.']);
        }
        $this->auth = new AuthManager($pdo);
    }

    public function tk_login($email, $pass, $remember = FALSE)
    {
        // global $tekoauth;
        $remember = ($remember) ? ((int) (60 * 60 * 24 * 365.25)) : null;
        try {
            if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
                $this->auth->login($email, $pass, $remember);
            } else {
                $this->auth->loginWithUsername($email, $pass, $remember);
            }
            return array(['error' => FALSE, 'id' => $this->auth->getUserId()]);
        } catch (InvalidEmailException $e) {
            return array('error' => TRUE, 'type' => 'email', 'message' => 'Invalid e-mail');
        } catch (InvalidPasswordException $e) {
            return array('error' => TRUE, 'type' => 'password', 'message' => 'Invalid password');
        } catch (EmailNotVerifiedException $e) {
            return array('error' => TRUE, 'type' => 'email_not_verified', 'message' => 'Your user has not yet been verified, check your inbox');
        } catch (TooManyRequestsException $e) {
            return array('error' => TRUE, 'type' => 'too_many_requests', 'message' => 'Too many requests, try again in %s', gmdate("H:i:s", $e->getCode()));
        } catch (UnknownUsernameException $e) {
            return array('error' => TRUE, 'type' => 'unknow_user', 'message' => 'Incorrect username or password');
        } catch (\Exception $e) {
            return array('error' => TRUE, 'type' => 'unknow', 'message' => 'Unknow error: %s', $e->getMessage(), 'e' => $e);
        }
    }

    function tk_register($email, $pass, $role = 'user', $username = null, $verify = false, $extra = [])
    {
        global $token;
        try {
            if ($verify) {
                $id = $this->auth->register($email, $pass, $username, function ($selector, $token) {
                    $GLOBALS['token'] = urlencode(base64_encode(json_encode(compact('selector', 'token'))));
                });
            } else {
                $id = $this->auth->admin()->createUser($email, $pass, $username);
            }
            if (count($extra)) {
                $extra['role'] = $role;
                $user          = UserModel::find($id);
                $user->update($extra);
            } else {
                $user       = UserModel::find($id);
                $user->role = $role;
                $user->save();
            }

            return ($verify) ? array('error' => FALSE, 'id' => $id, 'token' => $token) : array('error' => FALSE, 'id' => $id);
        } catch (InvalidEmailException $e) {
            return array('error' => TRUE, 'type' => 'email', 'message' => 'Invalid e-mail');
        } catch (InvalidPasswordException $e) {
            return array('error' => TRUE, 'type' => 'password', 'message' => 'Invalid password');
        } catch (UserAlreadyExistsException $e) {
            return array('error' => TRUE, 'type' => 'username_exists', 'message' => 'The username already exists');
        } catch (TooManyRequestsException $e) {
            return array('error' => TRUE, 'type' => 'too_many_requests', 'message' => 'Too many requests, try again in %s', gmdate("H:i:s", $e->getCode()));
        } catch (\Exception $e) {
            return array('error' => TRUE, 'type' => 'unknow', 'message' => 'Unknow error: %s', $e->getMessage(), 'e' => $e);
        }
    }

    //Get current user id
    function tk_id()
    {
        if ($this->auth->isLoggedIn()) {
            return $this->auth->getUserId();
        } else {
            return 0;
        }
    }

    //Get current user
    function tk_current_user()
    {
        $id = $this->tk_id();
        return UserModel::find($id);
    }

    //E-mail confirm
    function tk_confirm($token)
    {
        try {
            $partes = json_decode(base64_decode(urldecode($token)));
            $this->auth->confirmEmail($partes->selector, $partes->token);
            return array('error' => FALSE);
        } catch (InvalidSelectorTokenPairException $e) {
            return array('error' => TRUE, 'type' => 'invalid_token', 'message' => 'Invalid token selector');
        } catch (TokenExpiredException $e) {
            return array('error' => TRUE, 'type' => 'expired_token', 'message' => 'Token expired');
        } catch (TooManyRequestsException $e) {
            return array('error' => TRUE, 'type' => 'too_many_requests', 'messages' => 'Too many requests, try again in %s', gmdate("H:i:s", $e->getCode()));
        } catch (\Exception $e) {
            return array('error' => TRUE, 'type' => 'unknow', 'message' => 'Unknow error: %s', $e->getMessage(), 'e' => $e);
        }
    }

    //password Perdida
    function tk_recover($email)
    {
        global $token;
        try {
            $token = '';
            $this->auth->forgotPassword($email, function ($selector, $token) {
                $GLOBALS['token'] = urlencode(base64_encode(json_encode(compact('selector', 'token'))));
            });
            return array('error' => FALSE, 'token' => $token);
        } catch (InvalidEmailException $e) {
            return array('error' => TRUE, 'type' => 'email', 'message' => 'Invalid e-mail');
        } catch (TooManyRequestsException $e) {
            return array('error' => TRUE, 'type' => 'too_many_requests', 'message' => 'Too many requests, try again in %s', gmdate("H:i:s", $e->getCode()));
        } catch (\Exception $e) {
            return array('error' => TRUE, 'type' => 'unknow', 'message' => 'Unknow error: %s', $e->getMessage(), 'e' => $e);
        }
    }

    //Reset Password
    function tk_reset_password($token, $password)
    {
        try {
            $partes = json_decode(base64_decode(urldecode($token)));
            $this->auth->resetPassword($partes->selector, $partes->token, $password);
            return array('error' => FALSE);
        } catch (InvalidSelectorTokenPairException $e) {
            return array('error' => TRUE, 'type' => 'invalid_token', 'message' => 'Invalid token selector');
        } catch (TokenExpiredException $e) {
            return array('error' => TRUE, 'type' => 'expired_token', 'message' => 'Token expired');
        } catch (InvalidPasswordException $e) {
            return array('error' => TRUE, 'type' => 'password', 'message' => 'Invalid password');
        } catch (TooManyRequestsException $e) {
            return array('error' => TRUE, 'type' => 'too_many_requests', 'message' => 'Too many requests, try again in %s', gmdate("H:i:s", $e->getCode()));
        } catch (\Exception $e) {
            return array('error' => TRUE, 'type' => 'unknow', 'message' => 'Unknow error: %s', $e->getMessage(), 'e' => $e);
        }
    }

    //Change current user password
    function tk_change_current_password($antigua, $nueva)
    {
        try {
            $this->auth->changePassword($antigua, $nueva);
            return array('error' => FALSE);
        } catch (NotLoggedInException $e) {
            return array('error' => TRUE, 'type' => 'not_logged_in', 'message' => 'Not logged in');
        } catch (InvalidPasswordException $e) {
            return array('error' => TRUE, 'type' => 'password', 'message' => 'Invalid password');
        } catch (\Exception $e) {
            return array('error' => TRUE, 'type' => 'unknow', 'message' => 'Unknow error: %s', $e->getMessage(), 'e' => $e);
        }
    }

    //Change password by username
    function tk_change_password($password, $username = FALSE)
    {
        $id             = ($username) ? $username : $this->tk_id();
        $password       = password_hash($password, PASSWORD_DEFAULT);
        $user           = UserModel::find($id);
        $user->password = $password;
        $user->save();
        // if($res !== FALSE){
        //     return array('error' => FALSE, 'message' => __('Password changed successfully'));
        // } else {
        //     return array('error' => TRUE, 'message' => __('An error occurred when changing the password'));
        // }
    }

    //Log out
    function tk_logout()
    {
        $this->auth->logout();
    }
}
