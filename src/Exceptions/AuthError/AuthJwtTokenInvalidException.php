<?php

namespace TekoEstudio\Auth\Exceptions\AuthError;

use JetBrains\PhpStorm\Pure;
use TekoEstudio\Auth\Exceptions\AuthException;

class AuthJwtTokenInvalidException extends AuthException
{
    /**
     * Exception constructor.
     */
    #[Pure]
    public function __construct()
    {
        parent::__construct('Token inválido, ingresa nuevamente por favor', 403, 'expired_token');
    }
}