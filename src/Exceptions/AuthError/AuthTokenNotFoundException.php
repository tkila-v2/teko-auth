<?php

namespace TekoEstudio\Auth\Exceptions\AuthError;

use JetBrains\PhpStorm\Pure;
use TekoEstudio\Auth\Exceptions\AuthException;

class AuthTokenNotFoundException extends AuthException
{
    /**
     * Exception contractor.
     */
    #[Pure]
    public function __construct()
    {
        parent::__construct('Authorization token required', 422);
    }
}