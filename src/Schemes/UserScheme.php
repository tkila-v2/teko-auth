<?php

namespace TekoEstudio\Auth\Schemes;

use Exception;
use support\AuthJWT;
use TekoEstudio\Auth\Exceptions\AuthError\AuthJwtTokenInvalidException;
use TekoEstudio\Auth\Models\UserModel;

class UserScheme
{
    /**
     * @var string
     */
    protected string $token;

    /**
     * @var int|null
     */
    protected int|null $id = null;

    /**
     * @var \TekoEstudio\Auth\Models\UserModel|null
     */
    protected ?UserModel $model = null;

    /**
     * @param string $token
     *
     * @throws \TekoEstudio\Auth\Exceptions\AuthError\AuthJwtTokenInvalidException
     */
    public function __construct(string $token)
    {
        $this->token = $token;

        try {
            $data = AuthJWT::GetData($token);

            if (!isset($data->id_user)) {
                throw new Exception();
            }

            $this->id = (int) $data->id_user;
            $this->setModel();

        } catch (Exception) {
            throw new AuthJwtTokenInvalidException();
        }
    }

    /**
     * @return void
     */
    private function setModel(): void
    {
        if ($this->id != null) {
            $this->model = UserModel::where('id', $this->id)->first();
        }
    }

    /**
     * @return string
     */
    public function getToken(): string
    {
        return $this->token;
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return \TekoEstudio\Auth\Models\UserModel|null
     */
    public function getModel(): ?UserModel
    {
        return $this->model;
    }
}